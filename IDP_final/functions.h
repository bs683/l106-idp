#include <Adafruit_MotorShield.h> // Library for the motor shield
#include "sensors.h"


#define SERVOPIN 8 // Set pin address of the servo


Adafruit_MotorShield AFMS = Adafruit_MotorShield(); // Create the motor shield object with the default I2C address
Adafruit_DCMotor *leftMotor = AFMS.getMotor(1); // Set left motor to be connected to port M1
Adafruit_DCMotor *rightMotor = AFMS.getMotor(4); // Set right motor to be connected to port M2


const int calibrationSpeed = 35;
const int turningSpeed = 120;
const int searchTime = 2000; // Set how long robot needs to serach once pivot sensor is away from white line, in milliseconds


void amber_blink(unsigned long* previousMillis, int* amberState) { // blink the amber LED
  unsigned long currentMillis = millis(); // set up a variable for the current clock value
  if (currentMillis - *previousMillis >= 500) {
    *previousMillis = currentMillis; // save the last time the LED blinked
    if (*amberState == LOW) // if the LED is off turn it on and vice-versa:
      *amberState = HIGH;
    else
      *amberState = LOW;
    digitalWrite(AMBERPIN, *amberState); // set amber LED to amberState to allow flashing
  }
}


void calibration(int* leftMotorSpeed, int* rightMotorSpeed) { // Function to take OPB data from the front and change motor speeds
  // if left sensor touches white line, then slow down the left motor, same for right sensor
  if (OPB2_detecting() == true and OPB3_detecting() == false) { // condition when left sensor touches white line
    *leftMotorSpeed -= calibrationSpeed ;
    *rightMotorSpeed += calibrationSpeed;
  }
  else if (OPB2_detecting() == false and OPB3_detecting() == true) { // condition when right sensor touches white line
    *leftMotorSpeed += calibrationSpeed;
    *rightMotorSpeed -= calibrationSpeed;
  }
}


void clip_speed(int* leftMotorSpeed, int* rightMotorSpeed) { // Function to ensure the correct range of speeds
  *leftMotorSpeed = (*leftMotorSpeed >= 0) ? *leftMotorSpeed : 0; // Make sure both speeds are greater than 0
  *rightMotorSpeed = (*rightMotorSpeed >= 0) ? *rightMotorSpeed : 0;
  *leftMotorSpeed = (*leftMotorSpeed <= 255) ? *leftMotorSpeed : 255; // Make sure both speeds are less than 255
  *rightMotorSpeed = (*rightMotorSpeed <= 255) ? *rightMotorSpeed : 255;
}


bool detect_cross() { // read inputs to detect if robot arrives at cross
  if (OPB2_detecting() and OPB3_detecting())
    return true;  // if it is detected, return true, else return false
  else
    return false;
}


bool block_ahead() { // determine if there is a block ahead of the robot
  if (IR1_reading() > 19 and IR1_reading() < 23) { // if the IR detector is at the critical value
    Serial.println("Block ahead");
    return true; // return true if block is ahead
  }
  else {
    Serial.println("No block");
    return false;
  }
}


bool classify_block() { // read the inputs from the OPB sensor to detect which type of block is held
  if (OPB4_detecting()) { // read the input from OPB4
    Serial.print("Coarse");
    return true; // if the block is coarse, return ture, if it's fine, return false
  }
  else { // if the block is coarse, return ture, if it's fine, return false
    Serial.print("Fine");
    return false;
  }
}


void pivot_on_line(int *leftMotorSpeed, int *rightMotorSpeed) {  
  // search back and forth if the pivot sensor moves away from white line to compensate the difference between motors
  // if it has moved to black area, then first search backwards until it backs to white line or reaches time limit
  // then search forward until the same condition, at the end, release both motors and return
  if (!OPB1_detecting()) { // condition if the optoswitch cannot detect the white line
    *leftMotorSpeed = turningSpeed; // Set the speed of the motors
    *rightMotorSpeed = turningSpeed;
    leftMotor->run(BACKWARD); // Set the direction of the motors
    rightMotor->run(BACKWARD);

    unsigned long startSearchingTime = millis(); // Set the current time for searching
    unsigned long previousMillis = millis(); // Set the current time for flashing LED
    int amberState = LOW; // Set the LED state

    while (!OPB1_detecting()) { // condition that the sensor still detects black instead of white
      amber_blink(&previousMillis, &amberState);
      unsigned long currentSearchingTime = millis();
      if (currentSearchingTime - startSearchingTime > searchTime)
        break;
    }

    leftMotor->run(RELEASE); // Release the motors
    rightMotor->run(RELEASE);
  }

  if (!OPB1_detecting()) { //the same condition, if it still couldn't find the white line

    *leftMotorSpeed = turningSpeed;
    *rightMotorSpeed = turningSpeed;
    leftMotor->run(FORWARD);
    rightMotor->run(FORWARD);

    unsigned long startSearchingTime = millis();
    unsigned long previousMillis = millis();
    int amberState = LOW;

    while (!OPB1_detecting()) { // condition that the sensor still detects black instead of white
      amber_blink(&previousMillis, &amberState);
      unsigned long currentSearchingTime = millis();
      if (currentSearchingTime - startSearchingTime > 2 * searchTime)
        break;

    leftMotor->run(RELEASE); // stop the motora
    rightMotor->run(RELEASE);
  }
}
