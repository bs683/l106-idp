#include "sensors.h"
#include "functions.h"
#include <Servo.h> // Library for servo operation

const int navigationSpeedLeft = 238; // NavigationSpeed of both motors
const int navigationSpeedRight = 220;
const int IRDetectionTime = 1800;
const int correctiveDelay = 300;
const int degreeDelay90 = 35; // Time to allow robot to turn 90/180 degrees
const int degreeDelay180 = 80;
const int delay45 = 2500; // Time to allow robot to turn left 45 degrees
const int crossDelayTime = 300; // Delay after robot has detected cross
const int placementTime = 1000;
const int servoGrabPosition = 40; // Servo postion to allow servo release and grab
const int servoSafePosition = 80;
const int pickingBlockDelay = 100;
const int dropDelay = 200; // Wait to drop block
const int rampCrossing = 5000;
const int differenceThreshold = 6;
const int forwardingTime = 50;
const int backTime = 40;
const int startDelay = 200;


Servo idpservo; // Create the servo object to control mechanical arms


void forward_navigation_1(int* leftMotorSpeed, int* rightMotorSpeed, bool* holdingBlock, int* stage) { // primary forward navigation algorithm
  Serial.println("Moving forward.");
  
  digitalWrite(REDPIN, LOW); // set the indicator LED's to LOW state
  digitalWrite(GREENPIN, LOW);
  
  unsigned long previousMillis = millis(); // set the amber LED parameters
  int amberState = LOW;
  
  while (block_ahead() == false) {
    amber_blink(&previousMillis, &amberState);
    
    *leftMotorSpeed = navigationSpeedLeft;   // set both motors to naviagtion speed
    *rightMotorSpeed = navigationSpeedRight;
    
    calibration(leftMotorSpeed, rightMotorSpeed); // calibrate speed of both motors
    clip_speed(leftMotorSpeed, rightMotorSpeed); // clip motor speeds within 0 to 255
    
    leftMotor->setSpeed(*leftMotorSpeed); // set speeds of motors
    rightMotor->setSpeed(*rightMotorSpeed);
    leftMotor->run(FORWARD); // set the motors to run forwards
    rightMotor->run(FORWARD);
  }
  
  delay(IRDetectionTime); // delay to allow the robot to end up behind the block
  leftMotor->run(RELEASE); // stop the motors
  rightMotor->run(RELEASE);
  *stage += 1; // increment stage counter
}


void robot_turning(int turnDegree, bool isCoarse, bool holdingBlock, int* leftMotorSpeed, int* rightMotorSpeed, int* stage) { // allow robot to turn -90/90/180 degrees anticlockwise
  
  if (holdingBlock) { // If the robot is holding a block turn the indicator LED's on
    if (isCoarse) { // If the robot has a coarse block turn on the red light
      digitalWrite(REDPIN, HIGH); // digitalWrite to HIGH
      digitalWrite(GREENPIN, LOW);
    }
    else {
      digitalWrite(GREENPIN, HIGH);
      digitalWrite(REDPIN, LOW);
    }
  } 
  else {
    digitalWrite(REDPIN, LOW);
    digitalWrite(GREENPIN, LOW);     
  }
  
  unsigned long previousMillis = millis(); // set the last time for amber blinking
  int amberState = LOW; // set the state of the amber LED

  if (turnDegree == 90) { // turn 90 degrees anticlockwise
    int turncounter = 0;

    while (turncounter < degreeDelay90 || !OPB2_detecting()) { // wait for the forced turn, approximately 170 degrees
      amber_blink(&previousMillis, &amberState);
      
      *leftMotorSpeed = turningSpeed; // set the motor speeds for turning
      *rightMotorSpeed = turningSpeed;
      leftMotor->setSpeed(*leftMotorSpeed);
      rightMotor->setSpeed(*rightMotorSpeed);
      leftMotor->run(BACKWARD); // determine the directions of the motors
      rightMotor->run(FORWARD);
      
      pivot_on_line(leftMotorSpeed, rightMotorSpeed); // search back and forth if the pivot sensor moves away from white line
      
      delay(100); // wait 100ms
      turncounter += 1; // increment the turn counter
    }
    
    while (OPB2_detecting()) // wait until the left OPB is over the line
      delay(20);
    
    leftMotor->run(FORWARD); // set the motors to go the other way
    rightMotor->run(BACKWARD);
    delay(correctiveDelay);// apply this for a returning corrective delay
    leftMotor->run(RELEASE); // stop the motors
    rightMotor->run(RELEASE);
    *stage += 1; // increment the stage counter
  }

  if (turnDegree == -90) { // turn 90 degrees clockwise
    int turncounter = 0;

    while (turncounter < degreeDelay90 || !OPB3_detecting()) { // wait for the forced turn, approximately 170 degrees
      amber_blink(&previousMillis, &amberState);
      
      *leftMotorSpeed = turningSpeed; // set the motor speeds for turning
      *rightMotorSpeed = turningSpeed;
      leftMotor->setSpeed(*leftMotorSpeed);
      rightMotor->setSpeed(*rightMotorSpeed);
      leftMotor->run(FORWARD); // determine the directions of the motors
      rightMotor->run(BACKWARD);
      
      pivot_on_line(leftMotorSpeed, rightMotorSpeed); // search back and forth if the pivot sensor moves away from white line
      
      delay(100); // wait 100ms
      turncounter += 1; // increment the turn counter
    }
    
    while (OPB3_detecting()) // wait until the left OPB is over the line
      delay(20);
    
    leftMotor->run(BACKWARD); // set the motors to go the other way
    rightMotor->run(FORWARD);
    delay(correctiveDelay);// apply this for a returning corrective delay
    leftMotor->run(RELEASE); // stop the motors
    rightMotor->run(RELEASE);
    *stage += 1; // increment the stage counter
  }
  
  if (turnDegree == 180) { // turn 180 degrees anticlockwise
    int turncounter = 0;

    while (turncounter < degreeDelay180 || !OPB2_detecting()) { // wait for the forced turn, approximately 170 degrees
      amber_blink(&previousMillis, &amberState);
      
      *leftMotorSpeed = turingSpeed; // set the motor speeds for turning
      *rightMotorSpeed = turningSpeed;
      leftMotor->setSpeed(*leftMotorSpeed);
      rightMotor->setSpeed(*rightMotorSpeed);
      leftMotor->run(BACKWARD); // determine the directions of the motors
      rightMotor->run(FORWARD);
      
      pivot_on_line(leftMotorSpeed, rightMotorSpeed); // search back and forth if the pivot sensor moves away from white line
      
      delay(100); // wait 100ms
      turncounter += 1; // increment the turn counter
    }
    
    while (OPB2_detecting()) // wait until the left OPB is over the line
      delay(20);
    
    leftMotor->run(FORWARD); // set the motors to go the other way
    rightMotor->run(BACKWARD);
    delay(correctiveDelay);// apply this for a returning corrective delay
    leftMotor->run(RELEASE); // stop the motors
    rightMotor->run(RELEASE);
    *stage += 1; // increment the stage counter
  }
}


void acquire_block(bool* isCoarse, bool* holdingBlock, int* stage) {  // Set servo to grab the block and turn robot 180 degrees
  Serial.println("Picking block.");
  
  delay(pickingBlockDelay); // classify the type of the block while robot is held stationary
  *isCoarse = classify_block();
  Serial.println(*isCoarse);
  idpservo.write(servoGrabPosition); // grab the block with the servo
  *holdingBlock = true; // set the holdingBlock variable to true
  *stage += 1; // increment the stage counter
}


void backward_navigation(bool* holdingBlock, bool* isCoarse, int* leftMotorSpeed, int* rightMotorSpeed, int* stage) {
  Serial.println("Moving backward.");
  
  unsigned long previousMillis = millis(); // amber parameters
  int amberState = LOW;
  
  if (*holdingBlock) { // if holding block light LED indicators
    if (*isCoarse) { // if coarse show red LED
      digitalWrite(REDPIN, HIGH);
      digitalWrite(GREENPIN, LOW);
    }
    else { // else show green LED
      digitalWrite(GREENPIN, HIGH);
      digitalWrite(REDPIN, LOW);
    }
  }
  else {
    digitalWrite(GREENPIN, LOW);
    digitalWrite(REDPIN, LOW);
  }
  
  unsigned long startingTime = millis();
  while (!detect_cross() || abs(millis() - startingTime) < 3000) { // detect if robot arrives at the cross between red and blue squares
    amber_blink(&previousMillis, &amberState); // blink the amber LED
    
    *leftMotorSpeed = navigationSpeedLeft; // set both motors to negative naviagtion speed
    *rightMotorSpeed = navigationSpeedRight;
    
    calibration(leftMotorSpeed, rightMotorSpeed); // calibrate speed of both motors
    clip_speed(leftMotorSpeed, rightMotorSpeed); // clip motor speeds within 0 to 255
    
    leftMotor->setSpeed(*leftMotorSpeed); // set motor speeds
    rightMotor->setSpeed(*rightMotorSpeed);
    leftMotor->run(FORWARD); // set motor directions
    rightMotor->run(FORWARD);
  }
  
  delay(crossDelayTime); // move forward after cross is detected to let the third sensor pivot at the cross
  leftMotor->run(RELEASE); // stop motors
  rightMotor->run(RELEASE);
  *stage += 1; // increment stage counter
}


void allocate_block(int* leftMotorSpeed, int* rightMotorSpeed, bool* holdingBlock, int* stage) { // place the block in a square
  Serial.println("Putting block.");
  
  unsigned long previousMillis = millis(); // amber LED parameters
  int amberState = LOW;

  long startTime = millis(); // counter num of detected crosses
  while (millis() - startTime < placementTime) { // robot stops at the second cross (where the square is)
    amber_blink(&previousMillis, &amberState);
    
    *leftMotorSpeed = navigationSpeedLeft;   // set both motors to naviagtion speed
    *rightMotorSpeed = navigationSpeedRight;
    
    calibration(leftMotorSpeed, rightMotorSpeed); // calibrate speed of both motors
    clip_speed(leftMotorSpeed, rightMotorSpeed);
    
    leftMotor->setSpeed(*leftMotorSpeed); // else output speed to motors
    rightMotor->setSpeed(*rightMotorSpeed);
    leftMotor->run(FORWARD); // set motor direction
    rightMotor->run(FORWARD);
  }
  
  leftMotor->run(RELEASE);// stop motors
  rightMotor->run(RELEASE);
  
  delay(dropDelay); // wait to drop block
  idpservo.write(servoSafePosition); // release the block
  *holdingBlock = false; // set the holding_block variable to false
  digitalWrite(REDPIN, LOW); // turn off indicator LED's
  digitalWrite(GREENPIN, LOW);
  
  startTime = millis(); // reset starttime
  
  while (millis() - startTtime < placementTime - 200 and !OPB1_detecting()) { // robot stops at the second cross (where the square is)
    amber_blink(&previousMillis, &amberState);
    
    *leftMotorSpeed = navigationSpeedLeft;   // set both motors to naviagtion speed
    *rightMotorSpeed = navigationSpeedRight;
    
    calibration(leftMotorSpeed, rightMotorSpeed); // calibrate speed of both motors
    clipSpeed(leftMotorSpeed, rightMotorSpeed);
    
    leftMotor->setSpeed(*leftMotorSpeed); // else output speed to motors
    rightMotor->setSpeed(*rightMotorSpeed);
    leftMotor->run(BACKWARD);
    rightMotor->run(BACKWARD);
  }
  
  delay(1000);
  leftMotor->run(RELEASE); // stop motors
  rightMotor->run(RELEASE);
  *stage += 1; // increment stage counters
}


void forward_navigation_2(int* leftMotorSpeed, int* rightMotorSpeed, int* stage) {
  Serial.println("Moving forward for the last block.");
  
  unsigned long previousMillis = millis(); // amber LED parameters
  int amberState = LOW;
  long starttime = millis(); // time the function began
  
  while (!detect_cross() || millis() - starttime == rampCrossing) { // robot stops at the second cross (where the square is)
    amber_blink(&previousMillis, &amberState);
    
    *leftMotorSpeed = navigationSpeedLeft;   // set both motors to naviagtion speed
    *rightMotorSpeed = navigationSpeedRight;
    
    calibration(leftMotorSpeed, rightMotorSpeed); // calibrate speed of both motors
    clip_speed(leftMotorSpeed, rightMotorSpeed); // clip motor speeds within 0 to 255
    
    leftMotor->setSpeed(*leftMotorSpeed); // set motor speeds
    rightMotor->setSpeed(*rightMotorSpeed);
    leftMotor->run(FORWARD); // set motor directions
    rightMotor->run(FORWARD);
  }
  
  leftMotor->run(RELEASE); // stop motors
  rightMotor->run(RELEASE);
  *stage += 1; // increment stage counter
}


void search_block(int* leftMotorSpeed, int* rightMotorSpeed, bool* holdingBlock, bool* isCoarse, int* stage) {
  Serial.print("Searching for block.");

  int blockPosition1 = 0;
  int blockPosition2 = 0;

  unsigned long previousMillis = millis();
  int amberState = LOW;

  float distances[90];
  for (int i = 0; i < 90; i++) {
    distances[i] = 0.0;
  }

  *leftMotorSpeed = turningSpeed;
  *rightMotorSpeed = turningSpeed;
  leftMotor->setSpeed(*leftMotorSpeed);
  rightMotor->setSpeed(*rightMotorSpeed);
  leftMotor->run(BACKWARD);
  rightMotor->run(FORWARD);

  delay(delay45);

  for (int i = 0; i < 90; i++) {
    *leftMotorSpeed = turningSpeed;
    *rightMotorSpeed = turningSpeed;
    leftMotor->setSpeed(*leftMotorSpeed);
    rightMotor->setSpeed(*rightMotorSpeed);
    leftMotor->run(FORWARD);
    rightMotor->run(BACKWARD);

    distances[i] = IR1_reading();

    delay(40);
  }

  int counter = 0;

  for (int i = 0; i < 89; i++) {
    float difference = abs(distances[i + 1] - distances[i]);

    if (difference > differenceThreshold) {
      
      Serial.print("Block detected at ");
      Serial.print(i);
      Serial.print(" degrees, difference ");
      Serial.println(difference);

      if (counter == 0) {
        blockPosition1 = i;
        counter++;
      }
      if (counter == 1)
        blockPosition2 = i;
    }
  }

  int avgPosition = (blockPosition1 + blockPosition2) / 2;

  for (int i = 0; i < 90 - avgPosition; i++) {
    *leftMotorSpeed = turningSpeed;
    *rightMotorSpeed = turningSpeed;
    leftMotor->setSpeed(*leftMotorSpeed);
    rightMotor->setSpeed(*rightMotorSpeed);
    leftMotor->run(BACKWARD);
    rightMotor->run(FORWARD);
    delay(50);
  }

  for (int i = 0; i < forwardingTime; i++) {
    *leftMotorSpeed = navigationSpeedLeft;
    *rightMotorSpeed = navigationSpeedRight;
    leftMotor->setSpeed(*leftMotorSpeed);
    rightMotor->setSpeed(*rightMotorSpeed);
    leftMotor->run(FORWARD);
    rightMotor->run(FORWARD);
    delay(50);
  }

  leftMotor->run(RELEASE);
  rightMotor->run(RELEASE);

  acquire_block(isCoarse, holdingBlock, stage);

  for (int i = 0; i < forwardingTime - 5; i++) {
    *leftMotorSpeed = navigationSpeedLeft;
    *rightMotorSpeed = navigationSpeedRight;
    leftMotor->setSpeed(*leftMotorSpeed);
    rightMotor->setSpeed(*rightMotorSpeed);
    leftMotor->run(BACKWARD);
    rightMotor->run(BACKWARD);
    delay(50);
  }

  while (!OPB1_detecting()) {
    *leftMotorSpeed = navigationSpeedLeft;
    *rightMotorSpeed = navigationSpeedRight;
    leftMotor->setSpeed(*leftMotorSpeed);
    rightMotor->setSpeed(*rightMotorSpeed);
    leftMotor->run(BACKWARD);
    rightMotor->run(BACKWARD);
  }

  for (int i = 0; i < 225 - avgPosition; i++) {
    *leftMotorSpeed = turningSpeed;
    *rightMotorSpeed = turningSpeed;
    leftMotor->setSpeed(*leftMotorSpeed);
    rightMotor->setSpeed(*rightMotorSpeed);
    leftMotor->run(FORWARD);
    rightMotor->run(BACKWARD);
    delay(50);
  }

  while (!OPB3_detecting()) {
    *leftMotorSpeed = turningSpeed;
    *rightMotorSpeed = turningSpeed;
    leftMotor->setSpeed(*leftMotorSpeed);
    rightMotor->setSpeed(*rightMotorSpeed);
    leftMotor->run(FORWARD);
    rightMotor->run(BACKWARD);
  }

}


void move_out(int* leftMotorSpeed, int* rightMotorSpeed, int* stage) { // leave the starting square
  Serial.println("Moving out.");
  
  unsigned long previousMillis = millis(); // amber LED parameters
  int amberState = LOW;
  long startTime = millis(); // time counter
  
  while (millis() - startTime < startDelay) { // while in the start time interval
    amber_blink(&previousMillis, &amberState); // blink the amber LED
    
    *leftMotorSpeed = navigationSpeedLeft; // Set the motor speeds
    *rightMotorSpeed = navigationSpeedRight;
    
    leftMotor->setSpeed(*leftMotorSpeed);
    rightMotor->setSpeed(*rightMotorSpeed);
    leftMotor->run(FORWARD); // Set the motor direction
    rightMotor->run(FORWARD);
  }
  
  *stage += 1; // increment the stage counter
}


void move_in(int* leftMotorSpeed, int* rightMotorSpeed, int* stage) { // return to start box
  
  for (int i = 0; i < BACKTIME; i++) {
    *leftMotorSpeed = navigationSpeedLeft; // set motor speeds
    *rightMotorSpeed = navigationSpeedRight;
    
    leftMotor->setSpeed(*leftMotorSpeed);
    rightMotor->setSpeed(*rightMotorSpeed);
    leftMotor->run(FORWARD); // set motor directions
    rightMotor->run(FORWARD);
    delay(50);
  }
  
  leftMotor->run(RELEASE); // stop the motors
  rightMotor->run(RELEASE);
  *stage += 1; // increment stage counter
}


void stop_robot() { // stop the robot
  leftMotor->run(RELEASE);
  rightMotor->run(RELEASE);
}
