// Include required libraries
#include <RunningAverage.h> // Library for calculating runnning averages for smoothing


// Determine pinout
#define IRPIN1 A0 // Set pin address of infrared distance sensor
#define OPTOPIN1 5 // Set pin address of the optoswitch at back
#define OPTOPIN2 7 // Set pin address of the optoswitch at front left
#define OPTOPIN3 9 // Set pin address of the optoswitch at front right
#define OPTOPIN4 12 // Set pin address of the optoswitch for block classification
#define AMBERPIN 11 // Set pin address of the amber LED
#define REDPIN 4 // Set pin address of the red LED
#define GREENPIN 3 // Set pin address of the green LED
#define BUTTON 6 // Set pin address of the start button


RunningAverage OPB1(10); // Set up smoothing variables
RunningAverage OPB2(10);
RunningAverage OPB3(10);
RunningAverage OPB4(10);
RunningAverage IR1(10);


void setupPin() {
  pinMode(OPTOPIN1, INPUT); // set pins connected to optoswitches as inputs
  pinMode(OPTOPIN2, INPUT);
  pinMode(OPTOPIN3, INPUT);
  pinMode(OPTOPIN4, INPUT);
  pinMode(IRPIN1, INPUT); // set pins connected to infrared sensors as inputs
  pinMode(AMBERPIN, OUTPUT); // set pin connected to LEDs as outputs
  pinMode(REDPIN, OUTPUT);
  pinMode(GREENPIN, OUTPUT);
  pinMode(BUTTON, INPUT);
}


bool OPB1_detecting() { // Detect if white line is present under OPB
  OPB1.addValue(digitalRead(OPTOPIN1));
  smoothing_samples++;
  if (OPB1.getAverage() > 0.5) // if OPB average is over 0.5 then determine it to be 1
    return true;
  else
    return false; // if not determine it to be 0
}


bool OPB2_detecting() {
  OPB2.addValue(digitalRead(OPTOPIN2));
  smoothing_samples++;
  if (OPB2.getAverage() > 0.5)
    return true;
  else
    return false;
}


bool OPB3_detecting() {
  OPB3.addValue(digitalRead(OPTOPIN3));
  smoothing_samples++;
  if (OPB3.getAverage() > 0.5)
    return true;
  else
    return false;
}


bool OPB4_detecting() {
  OPB4.addValue(digitalRead(OPTOPIN4));
  smoothing_samples++;
  if (OPB4.getAverage() > 0.5)
    return true;
  else
    return false;
}


float IR1_reading() { // Detect distance to the robot
  float IRval = analogRead(IRPIN1) * 5.0 / 1023.0; // read voltage from sensor
  float IRdistance = (1.25 / 0.02) / IRval; // correction factor applied to set distance
  IR1.addValue(IRdistance); // add to the averaging variables

  return IR1.getAverage(); // return the average value from the last 10 measurements
}
