#include "sensors.h"
#include "functions.h"
#include "navigation.h"


// Initialise variables
int stage = 0; // Initialise stage counter
int leftMotorSpeed = 0; // Initialise speeds of both motors
int rightMotorSpeed = 0;
bool holdingBlock = false; // Indicate whether robot is holding a block
bool isCoarse = false; // Indicate which type of block robot is holding


void setup() { // put your setup code here, to run once:
  Serial.begin(9600); // Start the Serial monitor
  Serial.println("Test begin!");

  setupPin();

  idpservo.attach(SERVOPIN); // Attach a pin to the servo object
  idpservo.write(SERVOSAFEPOSITION); // Set the servo position to the safe position
  if (!AFMS.begin(100)) { // Start the motor shield at a frequency of 100Hz
    
    Serial.println("Could not find Motor Shield. Check wiring."); // Confirm success
    while (1);
  }

  while (digitalRead(BUTTON) == LOW ) { // Wait until the button is pressed to continue
    delay(100);
  }
}


void loop() {

  switch (stage) {
    case 0: // move out from starting square
      Serial.println("Case 0 ");
      move_out(&leftMotorSpeed, &rightMotorSpeed, &stage);
      break;
    case 1: // move forward for first block, stop when block detected
      Serial.println("Case 1 ");
      forward_navigation_1(&leftMotorSpeed, &rightMotorSpeed, &holdingBlock, &stage);
      break;
    case 2: // pick up a block
      Serial.println("Case 2 ");
      acquire_block(&isCoarse, &holdingBlock, &stage);
      break;
    case 3:// turn 180 degrees anticlockwise to face backwards
      Serial.println("Case 3 ");
      robot_turning(180, isCoarse, holdingBlock, &leftMotorSpeed, &rightMotorSpeed, &stage); 
      break;
    case 4: // move back until meeting the cross between red and blue squares
      Serial.println("Case 4 ");
      backward_navigation(&holdingBlock, &isCoarse, &leftMotorSpeed, &rightMotorSpeed, &stage);
      break;
    case 5:
      if (isCoarse) { // turn 90 degrees clockwise if the block is coarse
        Serial.println("Case 5a ");
        robot_turning(-90, isCoarse, holdingBlock &leftMotorSpeed, &rightMotorSpeed, &stage);
        }
      else if (!isCoarse) { // turn 90 degrees anticlockwise if the block is fine
        Serial.println("Case 5b ");
        robot_turning(90, isCoarse, holdingBlock, &leftMotorSpeed, &rightMotorSpeed, &stage);
      }
      break;
    case 6: // place block into the square and move back to the cross
      Serial.println("Case 6 ");
      allocate_block(&leftMotorSpeed, &rightMotorSpeed, &holdingBlock, &stage); 
      break;
    case 7: // turn 90 degrees to face forward again
      if (isCoarse) {
        Serial.println("Case 7a ");
        robot_turning(-90, isCoarse, holdingBlock &leftMotorSpeed, &rightMotorSpeed, &stage);
      }
      else if (!isCoarse) {
        Serial.println("Case 7b ");
        robot_turning(90, isCoarse, holdingBlock &leftMotorSpeed, &rightMotorSpeed, &stage);
      }
      break;
    case 8: // move forward for the second block, stop when block detected
      Serial.println("Case 8 ");
      forward_navigation_1(&leftMotorSpeed, &rightMotorSpeed, &holdingBlock, &stage); 
      break;
    case 9: // pick up a block
      Serial.println("Case 9");
      acquire_block(&isCoarse, &holdingBlock, &stage);
      break;
    case 10: // turn 180 degrees anticlockwise to face backwards
      Serial.println("Case 10 ");
      robot_turning(180, isCoarse, holdingBlock, &leftMotorSpeed, &rightMotorSpeed, &stage);
      break;
    case 11:// move back until meet the cross between red and blue squares
      Serial.println("Case 11 ");
      backward_navigation(&holdingBlock, &isCoarse, &leftMotorSpeed, &rightMotorSpeed, &stage);
      break;
    case 12:
      if (isCoarse) { // turn 90 degrees clockwise if the block is coarse
        Serial.println("Case 12a ");
        robot_turning(-90, isCoarse, holdingBlock &leftMotorSpeed, &rightMotorSpeed, &stage);
        }
      else if (!isCoarse) { // turn 90 degrees anticlockwise if the block is fine
        Serial.println("Case 12b ");
        robot_turning(90, isCoarse, holdingBlock &leftMotorSpeed, &rightMotorSpeed, &stage);
        }
      break;
    case 13: // put held block into the square and move back to the cross
      Serial.println("Case 13 ");
      allocate_block(&leftMotorSpeed, &rightMotorSpeed, &holdingBlock, &stage);
      break;
    case 14: // turn 90 degrees to face forward again
      if (isCoarse) { 
        Serial.println("Case 14a ");
        robot_turning(-90, isCoarse, holdingBlock &leftMotorSpeed, &rightMotorSpeed, &stage);
      }
      else if (!isCoarse) {
        Serial.println("Case 14b");
        robot_turning(90, isCoarse, holdingBlock, &leftMotorSpeed, &rightMotorSpeed, &stage);
      }
      break;
    case 15: // move forward for the last block, stop at a certain distance from the corner
      Serial.println("Case 15 ");
      forward_navigation_2(&leftMotorSpeed, &rightMotorSpeed, &stage);
      break;
    case 16: // scan 90 degrees for the the last block, move forward to pick it, then move back and turn to face backward
      Serial.println("Case 16 ");
      search_block(&leftMotorSpeed, &rightMotorSpeed, &holdingBlock, &isCoarse, &stage);
      break;
    case 17: // turn 180 degrees anticlockwise to face backwards
      Serial.println("Case 17 ");
      robot_turning(180, isCoarse, holdingBlock, &leftMotorSpeed, &rightMotorSpeed, &stage);
      break;
    case 18: // move back until meet the cross between red and blue square
      Serial.println("Case 18 ");
      backward_navigation(&holdingBlock, &isCoarse, &leftMotorSpeed, &rightMotorSpeed, &stage);
      break;
    case 19: // move to the required stage
      if (holdingBlock) { // if the robot is holding a block move to a turning stage
        if  (isCoarse) { // turn
          Serial.println("Case 19a ");
          robot_turning(-90, isCoarse, holdingBlock, &leftMotorSpeed, &rightMotorSpeed, &stage);
        }
        else if (!isCoarse) {
          Serial.println("Case 19b");
          robot_turning(90, isCoarse, holdingBlock, &leftMotorSpeed, &rightMotorSpeed, &stage);
        }
      }
      else { // if no block present return home
        stage = 22; // move to the returning home stage
      }
      break;
    case 20: // drop the block
      Serial.println("Case 19 ");
      allocate_block(&leftMotorSpeed, &rightMotorSpeed, &holdingBlock, &stage);
      break;
    case 21: // turn toward the start box
      if (isCoarse) {
        Serial.println("Case 20a ");
        robot_turning(90, isCoarse, holdingBlock, &leftMotorSpeed, &rightMotorSpeed, &stage);
      }
      else if (!isCoarse) {
        Serial.println("Case 20b ");
        robot_turning(-90, isCoarse, holdingBlock, &leftMotorSpeed, &rightMotorSpeed, &stage);
      }
      break;
    case 22: // return to home
      Serial.println("Case 21 ");
      move_in(&leftMotorSpeed, &rightMotorSpeed, &stage);
      break;
    case 23: // stop robot
      Serial.println("Case 22 ");
      stop_robot();
      break;
  }
}
